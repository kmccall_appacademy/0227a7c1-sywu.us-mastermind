class Code
  attr_reader :pegs
  PEGS = {"r"=>"red", "g"=>"green", "b"=>"blue", "y"=>"yellow", "o"=>"orange", "p"=>"purple"}
  def initialize(code)
    @pegs = code
  end
  
  def self.parse(guess_code)
    if guess_code.downcase.chars.all? {|letter| PEGS.include?(letter)}
      code =  guess_code.downcase.chars
      Code.new(code)
    else
      raise "Invalid Code"
    end
  end
  
  def self.random
    code = []
    until code.length == 4
      code << PEGS.keys[rand(0..5)]
    end
    self.parse(code.join)
  end
  
  def [](i)
    pegs[i]
    # debugger
  end
  
  def exact_matches(guess_code)
    count = 0
    (0..3).each do |index|
      count += 1 if self[index] == guess_code[index]
    end
    count
  end
  
  def near_matches(guess_code)
    count = 0
    code = self.pegs
    guess = guess_code.pegs
    (0..3).each do |index|
      if code[index] != guess[index] && guess.include?(code[index])
        i = guess.index(code[index])
        if code[i] != guess[i]
          guess[i] = nil
          count += 1
        end
      end
    end
    count
  end
  
  def == (guess_code)
    unless guess_code.instance_of?(Code)
      return false
    end
    self.pegs == guess_code.pegs
  end
  
end

class Game
  attr_reader :secret_code
  
  def initialize(code=Code.random)
    @secret_code = code
  end
  
  def get_guess
    puts "Enter guess"
    input = gets.chomp
    Code.parse(input)
  end
  
  def display_matches(code)
    exact_count = secret_code.exact_matches(code)
    near_count = secret_code.near_matches(code)
    puts "You have #{exact_count} exact matches"
    puts "You have #{near_count} near matches"
  end
end
